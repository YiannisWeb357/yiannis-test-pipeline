import { add, divide, multiply } from '../src/index';

test('add function adds two numbers', () => {
    expect(add(1, 2)).toBe(3);
    expect(add(-1, 2)).toBe(1);
});

test('divide function divides two numbers', () => {
    expect(divide(4, 2)).toBe(2);
    expect(divide(10, 2)).toBe(5);
});

test('multiply function multiplies two numbers', () => {
    expect(multiply(4, 2)).toBe(8);
    expect(multiply(10, 2)).toBe(20);
});
