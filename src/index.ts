// index.ts

function add(a: number, b: number): number {
    return a + b;
}

// Divide function
function divide(a: number, b: number): number {
    return a / b;
}

// Multiply function
function multiply(a: number, b: number): number {
    return a * b;
}

const myName: string = 'John';
const age: number = 30;
const test: string = 'test';
function greet(): string {
    return `Hello, ${myName}! Your aga is ${age}. Test: ${test}`;
}

greet();

export { add, divide, multiply };
